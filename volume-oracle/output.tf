output "id" {
  value = oci_core_volume.volume.id
}

output "volume_id" {
  value = oci_core_volume.volume.id
}

output "volume_name" {
  value = oci_core_volume.volume.display_name
}

output "volume_size" {
  value = var.volume_size
}
