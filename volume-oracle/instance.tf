resource "oci_core_volume" "volume" {
  compartment_id      = var.compartment_id
  availability_domain = var.availability_domain
  size_in_gbs         = var.volume_size
}