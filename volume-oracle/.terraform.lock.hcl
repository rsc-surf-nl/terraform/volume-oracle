# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/tls" {
  version     = "4.0.5"
  constraints = "~> 4.0.5"
  hashes = [
    "h1:e4LBdJoZJNOQXPWgOAG0UuPBVhCStu98PieNlqJTmeU=",
    "zh:01cfb11cb74654c003f6d4e32bbef8f5969ee2856394a96d127da4949c65153e",
    "zh:0472ea1574026aa1e8ca82bb6df2c40cd0478e9336b7a8a64e652119a2fa4f32",
    "zh:1a8ddba2b1550c5d02003ea5d6cdda2eef6870ece86c5619f33edd699c9dc14b",
    "zh:1e3bb505c000adb12cdf60af5b08f0ed68bc3955b0d4d4a126db5ca4d429eb4a",
    "zh:6636401b2463c25e03e68a6b786acf91a311c78444b1dc4f97c539f9f78de22a",
    "zh:76858f9d8b460e7b2a338c477671d07286b0d287fd2d2e3214030ae8f61dd56e",
    "zh:a13b69fb43cb8746793b3069c4d897bb18f454290b496f19d03c3387d1c9a2dc",
    "zh:a90ca81bb9bb509063b736842250ecff0f886a91baae8de65c8430168001dad9",
    "zh:c4de401395936e41234f1956ebadbd2ed9f414e6908f27d578614aaa529870d4",
    "zh:c657e121af8fde19964482997f0de2d5173217274f6997e16389e7707ed8ece8",
    "zh:d68b07a67fbd604c38ec9733069fbf23441436fecf554de6c75c032f82e1ef19",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/oracle/oci" {
  version     = "6.1.0"
  constraints = "~> 6.1.0"
  hashes = [
    "h1:T73Sl2gV/GDJTr7XdRZ3O2umz+AETU+LvTBEUW8+GpA=",
    "zh:045fbb938759a3d2877cf2f31ba4df620e1d8c748f51d651105f1f1dc155824c",
    "zh:30fba460a7de28d89ffa2aeb8177b0c80914d33b0ba2667b3577abb7552c708b",
    "zh:3e74ba00b5d0c6052a87e7da00793280baa3602a9ca64c5103300afdb2c38b0f",
    "zh:50795a5c4c34cd5a76a37d4c16b7391a5580536948b5f4e70ed75090ce09f6d8",
    "zh:592f6db758c1d1a955fb86b86bf219cc45edad1e7fd8c75aa330bba2c4337e7f",
    "zh:5c86bba37e3bb35a70818d7352d06e650e0f319eb0f8d89de7d3bd57ae7f9234",
    "zh:69ba6345c4d2e6bb2abf52352a4f272d3b768aafca338f3d142e98c878b40710",
    "zh:7e77d29f742d727c70b3bdf7a0815208144459a4efcd01f67782d6ea427f1ee3",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9dbf146e9a59c376cf8f688f03e40997260000b6db795fe36337fe120da2ffce",
    "zh:ada66f3826ee5e7bba1a77f9f3b3947be15326abb782350b6e0bc302c7b0cbd8",
    "zh:c4d11a7ce47361f89f19bc1704c5cea318f80440ea419145e7292c9a870b1046",
    "zh:cdce02f9bea45ca9d98cd9d4c0298002eb658dbe45a63771d734a51cf8c3c61c",
    "zh:d42fbceb82a4b553afbfa5cc5c976d15cabe9eccb83d1aaf3a28d2a1376b4559",
    "zh:fb54b6b52478a0152b80b5bee5a907b2aca0b9a2006a550efdd7d116f59ede7c",
  ]
}
